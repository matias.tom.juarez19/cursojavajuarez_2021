package moduloTESTEOS;

public class Resistencia extends Componente {

	public Resistencia() {
		super("R_def", 100, "ohm");
	}

	public Resistencia(String nombre, float valor) {
		super(nombre, valor, "ohm");		
	}

	@Override
	public float calcularImpedancia() {
		return getValor();
	}

}
