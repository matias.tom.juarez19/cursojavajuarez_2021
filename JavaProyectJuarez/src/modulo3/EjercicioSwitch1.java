package modulo3;

import java.util.Scanner;

public class EjercicioSwitch1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("Introduzca el mes");
		String Mes = scan.next();
		
		switch(Mes)
		{
		case "Febrero": case "febrero":
					System.out.println("Este mes tiene 28 d�as");
			break;
		case "Noviembre": case "noviembre": case "Abril": case "abril": case "Junio": case "junio": case "Septiembre": case "septiembre":
					System.out.println("Este mes tiene 30 d�as");
			break;
		case "Enero": case "enero": case "Marzo": case "marzo": case "Mayo": case "mayo": case "Julio": case "julio": case "Agosto": case "agosto": case "Octubre": case "octubre": case "Diciembre": case "diciembre":
					System.out.println("Este mes tiene 32 d�as");
			break;
		default:
					System.out.println("El mes introducido es inv�lido \nReinicie el programa e introduzaca un mes v�lido");
		}
		
		scan = null;
	}

}
