package modulo3;

import java.util.Scanner;

public class EjercicioCiclos1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Ingrese el n�mero del que quiera saber su tabla");
		Scanner scan = new Scanner(System.in);
		int Num = scan.nextInt();
		
		System.out.println("La tabla del n�mero "+Num+" es la siguiente");
		
		for (int v = 0; v <= 10; v++)
		{
			int resultado = Num * v;
			System.out.println(Num + " x " + v + " = " + resultado);
		}
		scan = null;
	}

}
