package modulo3;

import java.util.Scanner;

public class EjercicioCiclos2B {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Ingrese el n�mero del que quiera saber su tabla");
		Scanner scan = new Scanner(System.in);
		int Num = scan.nextInt();
		int SumImpar = 0;
		int SumTotal = 0;
		
		System.out.println("La tabla del n�mero "+Num+" es la siguiente");
		
		for (int v = 1; v <= 10; v++)
		{
			int Res = Num * v;
			System.out.println(Num + " x " + v + " = " + Res);
			int Par = Res % 2;
			SumTotal = SumTotal + Res;
			SumImpar = SumImpar + (Res * Par);
		}
		
		int SumPar = SumTotal - SumImpar;
		
		System.out.println("La suma de numeros pares tiene como resultado " + SumPar);
		scan = null;
	}

}
