package modulo3;

import java.util.Scanner;

public class EjerciciosIF3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese un mes");
		String Mes = scan.nextLine();
		

		if (Mes.equals("Febrero"))
		{
			System.out.println("El mes tiene 28 dias");
		}
		else if (Mes.equals("Enero") || Mes.equals("Marzo") || Mes.equals("Julio") || Mes.equals("Agosto") || Mes.equals("Octubre") || Mes.equals("Diciembre"))
		{
				System.out.println("El mes tiene 31 dias");
		}
		else if (Mes.equals("Abril") || Mes.equals("Junio") || Mes.equals("Septiembre") || Mes.equals("Noviembre"))
		{
			System.out.println("El mes tiene 30 dias");
		}
		else
		{
			System.out.println("El mes ingresado est� mal escrito\nReinicie el programa e int�ntelo nuevamente.");
		}
		scan = null;
//		Este c�digo (equals) me lo ense�� un compa�ero y me pareci� muy eficiente para este trabajo, as� que es probable vea que se parece a otro y crea que me copi�.
	}

}
