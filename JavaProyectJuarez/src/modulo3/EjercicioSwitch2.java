package modulo3;

import java.util.Scanner;

public class EjercicioSwitch2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("Introduzca el puesto del competidor");
		int Puesto = scan.nextInt();
		
		switch (Puesto)
		{
		case 1:
				System.out.println("Medalla de Oro");
			break;
		case 2:
				System.out.println("Medalla de Plata");
			break;
		case 3:
				System.out.println("Medalla de Bronce");
			break;
		default:
				System.out.println("Siga participando");
		}
		
		scan = null;
	}

}
