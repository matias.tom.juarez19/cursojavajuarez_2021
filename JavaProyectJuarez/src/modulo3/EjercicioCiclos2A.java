package modulo3;

import java.util.Scanner;

public class EjercicioCiclos2A {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Ingrese el n�mero del que quiera saber su tabla");
		Scanner scan = new Scanner(System.in);
		int Num = scan.nextInt();
		int SumPar = 0;
		
		System.out.println("La tabla del n�mero "+Num+" es la siguiente");
		
		for (int v = 1; v <= 10; v++)
		{
			int Res = Num * v;
			System.out.println(Num + " x " + v + " = " + Res);
			if (Res % 2 == 0)
				SumPar = SumPar + Res;
		}
		
		System.out.println("La suma de numeros pares tiene como resultado " + SumPar);
		
		scan = null;
	}

}
