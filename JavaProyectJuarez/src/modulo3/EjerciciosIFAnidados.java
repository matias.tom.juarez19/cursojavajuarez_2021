package modulo3;

import java.util.Scanner;

public class EjerciciosIFAnidados {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("Introduzca el n�mero de su curso");
		int curso = scan.nextInt();
		
		if (curso == 0)
			System.out.println("Jard�n de infantes");
		else if (curso >= 1 && curso <= 6)
			System.out.println("Primaria");
		else if (curso >= 7 && curso <= 12)
			System.out.println("Secundaria");
		else
			System.out.println("El n�mero de curso que se ingres� est� fuera del rango de este programa o es inv�lido\nDe ser necesario reinicie el programa");
		scan = null;
	}

}
