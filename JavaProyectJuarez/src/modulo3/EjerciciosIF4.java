package modulo3;

import java.util.Scanner;

public class EjerciciosIF4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese una de las siguientes letras:\nA, B o C");
		String cat = scan.nextLine();
		
		if (cat.equals("A") || cat.equals("a"))
			System.out.println("hijo");
		
		else if (cat.equals("B") || cat.equals("b"))
			System.out.println("padres");
		
		else if (cat.equals("C") || cat.equals("c"))
			System.out.println("abuelos");
		
		else
			System.out.println("La letra ingresada es inválida\nReinicie el programa e inténtelo nuevamente.");
		
		scan = null;
	}

}
