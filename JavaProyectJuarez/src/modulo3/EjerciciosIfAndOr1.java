package modulo3;

import java.util.Scanner;

public class EjerciciosIfAndOr1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("Para la siguiente competicia los valores son: \n Piedra = 0 \n Papel  = 1 \n Tijera = 2\n");
		System.out.println("Ingrese el valor del primer competidor");
		int Valor1=scan.nextInt();
		System.out.println("Ingrese el valor del segundo competidor");
		int Valor2=scan.nextInt();
		
		
		if (Valor1 == Valor2)
			System.out.println("Ambos competidores empataron");
		else
		
		{
			if (Valor1 == 0 && Valor2 == 2)
				System.out.println("Gano el primer competidor con Piedra");
			else if (Valor1 == 0 && Valor2 == 1)
				System.out.println("Gano el segundo competidor con Papel");
			else if (Valor1 == 1 && Valor2 == 0)
				System.out.println("Gano el primer competidor con Papel");
			else if (Valor1 == 1 && Valor2 == 2)
				System.out.println("Gano el segundo competidor con Tijera");
			else if (Valor1 == 2 && Valor2 == 0)
				System.out.println("Gano el segundo competidor con Piedra");
			else if (Valor1 == 2 && Valor2 == 1)
				System.out.println("Gano el primer competidor con Tijera");
		}
		
		scan = null;

	}

}
