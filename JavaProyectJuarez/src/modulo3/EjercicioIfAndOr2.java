package modulo3;

import java.util.Scanner;

public class EjercicioIfAndOr2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese el primer valor");
		int Valor1 =scan.nextInt();
		System.out.println("Ingrese la segunda valor");
		int Valor2 =scan.nextInt();
		System.out.println("Ingrese la tercera valor");
		int Valor3 =scan.nextInt();
		
		if (Valor1 > Valor2 && Valor1 > Valor3)
			System.out.println("La primer variable es la mayor con "+ Valor1);
		else if (Valor2 > Valor1 && Valor2 > Valor3)
			System.out.println("La segunda variable es la mayor con "+ Valor2);
		else if (Valor3 > Valor1 && Valor3 > Valor2)
			System.out.println("La tercera variable es la mayor con "+ Valor3);
		else
			System.out.println("No hay numero mayor");

		scan = null;

	}

}
