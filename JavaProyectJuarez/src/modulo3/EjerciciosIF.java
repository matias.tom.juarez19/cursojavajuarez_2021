package modulo3;

import java.util.Scanner;

public class EjerciciosIF {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese la primera nota");
		float Nota1 = scan.nextFloat();
		
		System.out.println("Ingrese la segunda nota");
		float Nota2 = scan.nextFloat();
		
		System.out.println("Ingrese la tercera nota");
		float Nota3 = scan.nextFloat();
		
		float promedio = (Nota1+Nota2+Nota3)/3;
		
		if (promedio>=7)
		{
				System.out.println("Good Ending: Aprobaste");
				System.out.println("Nota Final =\t" + promedio);
				System.out.println("CONGRATS, PASASTE LA CLASE DE CASAAAAS :D");
		}
		else 
		{
				System.out.println("Bad Ending: Desaprobaste");
				System.out.println("Nota Final =\t" + promedio);
				System.out.println("MISSION FAILED, WE'LL GET THEM NEXT TIME");
		}
		
		scan = null;
	}

}
