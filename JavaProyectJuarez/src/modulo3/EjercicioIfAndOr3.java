package modulo3;

import java.util.Scanner;

public class EjercicioIfAndOr3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan= new Scanner(System.in);
		System.out.println("Ingrese una Letra");
		char ABC = scan.next().charAt(0);
		
		if (ABC == 'a' || ABC == 'e'|| ABC == 'i'|| ABC == 'o'|| ABC == 'u' || ABC == 'A' || ABC == 'E' || ABC == 'I' || ABC == 'O' || ABC == 'U')
			System.out.println("Su letra es una vocal");
		else
			System.out.println("Su letra es una consonante o un valor incorrecto");
		
		scan = null;
	}

}
