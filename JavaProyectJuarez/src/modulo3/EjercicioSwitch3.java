package modulo3;

import java.util.Scanner;

public class EjercicioSwitch3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("Introduzca la clase de su automóvil A, B O C");
		char Clase = scan.next().charAt(0);
		
		switch (Clase)
		{
		case 'A':
				System.out.println("Su automóvil posee 4 ruedas y un motor");
			break;
		case 'B':
				System.out.println("Su automóvil posee 4 ruedas, un motor, cierre centralizado y aire");
			break;
		case 'C':
				System.out.println("Su automóvil posee 4 ruedas, un motor, cierre centralizado, aire y airbag");
			break;
		default:
				System.out.println("La clase que ha introducido es inválida \nReinice el programa e introduzca una clase válida");
		
		}
		scan = null;
	}

}
