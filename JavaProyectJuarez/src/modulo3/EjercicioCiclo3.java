package modulo3;


public class EjercicioCiclo3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int Num = 10;
		
		System.out.println("Las tablas hasta la del "+Num+" son las siguientes:\n");
		
		for (int N = 0; N < Num+1; N++)
		{
			for(int v = 1; v <= 10; v++)
			{
				int Res = N * v;
				System.out.println(N + " x " + v + " = " + Res);
			}
		}
	}

}
