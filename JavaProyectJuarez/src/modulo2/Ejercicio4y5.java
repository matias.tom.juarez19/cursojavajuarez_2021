package modulo2;

public class Ejercicio4y5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		Ejercicio 4
	
		byte b=10;
		short s=20;
		int i=30;
		long l=40;
		
		byte sumabb =  (byte) (b+b);
		short sumabs = (short) (b+s);
		int sumabi = b+i;
		int sumaii = i+i;
		long sumasl = s+l;
		
		System.out.println("El resultado de la suma entre Bytes es\t"+sumabb);
		System.out.println("El resultado de la suma entre Short y Byte es/t"+sumabs);
		System.out.println("El resultado de la suma entre Int y Byte es\t"+sumabi);
		System.out.println("El resultado de la suma entre Ints es\t"+sumaii);
		System.out.println("El resultado de la suma entre Short y Long es\t"+sumasl);
		
//		Ejercicio 5
		
		l = s;
		b= (byte)(s);
		// No es posible
		l=i;
		b= (byte) (i);
		// No es posible
		s= (short) (i);
		// No es posible

	}

}
