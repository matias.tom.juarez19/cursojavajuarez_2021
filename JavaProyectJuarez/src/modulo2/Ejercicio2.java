package modulo2;

public class Ejercicio2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		byte 		bmin = -128;
		byte 		bmax = 127;
		
//		reemplazar el 0 por el valor que corresponda en todos los caso
// 		F�rmula para:

		short 		smin = -32768;
		short 		smax = 32767;
		int 		imin = -2147483648;
		int 		imax = 2147483647;
		long 		lmin = -9223372036854775808L;
		long 		lmax = 9223372036854775807L;
		System.out.println("Tipo\tMinimo\t\t\tMaximo");
		System.out.println("....\t......\t\t\t......");
		System.out.println("\nbyte\t" + bmin + "\t\t\t" + bmax);
		System.out.println("\nshort\t" + smin + "\t\t\t" + smax);
		System.out.println("\nint\t" + imin + "\t\t" + imax);
		System.out.println("\nlong\t" + lmin + "\t" + lmax);
		
//		M�nimos: -(2^CantBits)/2
//		M�ximos: (2^CantBits)/2 - 1
		
	}

}
